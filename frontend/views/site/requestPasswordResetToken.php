<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use kartik\icons\Icon;
Icon::map($this);

$this->title = 'Restablecer contraseña';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset">

    <div class="row">

        <div class="col-lg-3"></div>

        <div class="col-lg-6" style="background-color: #ffffff; margin: 0.5rem 0 1rem 0; border-radius: 2px; box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);">

            <div style="padding: 24px;">

                <h3><?= Icon::Show('key').Html::encode($this->title) ?></h3>
                <i><h6>Ingrese su correo electrónico. Recibirá un enlace para restablecer la contraseña.</h6></i>
                <hr>

                <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                <h6><?= $form->field($model, 'email', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->textInput(['autofocus' => true])->label('E-mail') ?></h6>

                <hr>

                <div class="form-group">
                    <p align="right"><?= Html::submitButton('Enviar', ['class' => 'btn btn-default']) ?></p>
                </div>

                <?php ActiveForm::end(); ?>

            </div>

        </div>

        <div class="col-lg-3"></div>

    </div>

</div>
