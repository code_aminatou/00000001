<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use kartik\icons\Icon;
Icon::map($this);

$this->title = 'Registrarse';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">

    <div class="row">

        <div class="col-lg-3"></div>

        <div class="col-lg-6" style="background-color: #ffffff; margin: 0.5rem 0 1rem 0; border-radius: 2px; box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);">

            <div class="row">

                <div>

                    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                    <div style="padding: 24px;">

                        <h3><?= Icon::Show('address-card').Html::encode($this->title) ?></h3>
                        <hr>
                        <div class="row">

                                <div class="col-lg-6">
                                    <?= $form->field($model, 'nombre_real', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->textInput(['autofocus' => true, 'placeholder' => 'Nombre'])->label('Nombre') ?>
                                </div>
                                
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'apellido_real', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->textInput(['placeholder' => 'Apellido'])->label('Apellido') ?>
                                </div>

                        </div>

                        <div class="row">

                                <div class="col-lg-6">
                                    <?= $form->field($model, 'dni', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->textInput(['readonly' => false, 'placeholder' => '000000000'])->label('DNI (Solo ingrese números)') ?>
                                </div>

                                <div class="col-lg-6">
                                    <?= $form->field($model, 'genero', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->radioList(array('1' => 'Masculino', '2' => 'Femenino'))->label('Género') ?>
                                </div>

                        </div>

                        <hr>

                        <div class="row">

                                <div class="col-lg-6">
                                    <?= $form->field($model, 'username', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->textInput(['placeholder' => 'Nombre de usuario'])->label('Nombre de usuario') ?>
                                </div>

                                <div class="col-lg-6">
                                    <?= $form->field($model, 'password', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->passwordInput(['placeholder' => 'Contraseña'])->label('Contraseña') ?>
                                </div>

                        </div>

                        <?= $form->field($model, 'email', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->textInput(['placeholder' => 'a@a.com'])->label('E-mail') ?>
                        
                        <!-- Campos ocultos -->
                        <?= $form->field($model, 'rol')->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => '1'])->label(false); ?>
                        <?= $form->field($model, 'codigo_empresa')->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => '1'])->label(false); ?>

                        <div class="form-group">
                            <p align="right"><?= Html::submitButton('Registrarse', ['class' => 'btn btn-default', 'name' => 'signup-button']) ?></p>
                        </div>

                    </div>
                    <?php ActiveForm::end(); ?>

                </div>

            </div>

        </div>

        <div class="col-lg-3"></div>

    </div>
</div>
