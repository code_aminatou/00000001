<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use kartik\icons\Icon;
Icon::map($this);

$this->title = 'Reenviar enlace de validación de e-mail';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-resend-verification-email">

    <div class="row">

        <div class="col-lg-3"></div>

        <div class="col-lg-6" style="background-color: #ffffff; margin: 0.5rem 0 1rem 0; border-radius: 2px; box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);">

            <?php $form = ActiveForm::begin(['id' => 'resend-verification-email-form']); ?>
            <div style="padding: 24px;">

                <h3><?= Icon::Show('envelope').Html::encode($this->title) ?></h3>
                <hr>

                <h6>
                <?= $form->field($model, 'email', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->textInput(['autofocus' => true])->label('Ingrese su e-mail') ?>

                </h6>
                <div class="form-group">
                    <p align="right"><?= Html::submitButton('Reenviar enlace', ['class' => 'btn btn-default']) ?></p>
                </div>

            </div>
            <?php ActiveForm::end(); ?>

            <div class="col-lg-3"></div>
        
        </div>
    </div>
</div>
