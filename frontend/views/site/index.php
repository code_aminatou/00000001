<?php
use common\models\Slider;
use common\models\ImgSlider;
use kv4nt\owlcarousel\OwlCarouselWidget;

/* @var $this yii\web\View */

$this->title = 'Inicio';
?>
<div class="site-index">

    <!-- Slider # 1: Inicio -->
    <?php

        $Slider_1_velocidad = 0;
        $Slider_1_img_cantidad = 0;
        $Slider_1_status = 0; // <--- OK.

        $Slider_1 = Slider::findOne(['numero_slider' => 1]);

        if (isset($Slider_1)) {

            if ($Slider_1->status == 1) {

                // Cálculo de velocidad:
                if ($Slider_1->velocidad <= 0) {
                    $Slider_1_velocidad = 1000;
                } else {
                    $Slider_1_velocidad = $Slider_1->velocidad * 1000;
                }

                // Cantidad de imágenes visibles simultáneamente:
                if ($Slider_1->img_cantidad <= 0) {
                    $Slider_1_img_cantidad = 1;
                } else {
                    $Slider_1_img_cantidad = $Slider_1->img_cantidad;
                }

                OwlCarouselWidget::begin([
                    'container' => 'div',
                    'containerOptions' => [
                        'id' => 'container-id',
                        'class' => 'container-class'
                    ],
                    'pluginOptions'    => [
                        'autoplay'          => true,
                        'autoplayTimeout'   => $Slider_1_velocidad,
                        'items'             => $Slider_1_img_cantidad,
                        'loop'              => true,
                        'itemsDesktop'      => [1199, 3],
                        'itemsDesktopSmall' => [979, 3]
                    ]
                ]);

                $ImgBanner_All = ImgSlider::getAll_ImgSlider(1);

                foreach ($ImgBanner_All as $ImgBanner) {
                    echo '<div class="item-class"><a href="'.$ImgBanner->url.'"><img id="slider_1" src="'.Yii::$app->request->BaseUrl.'/../../archivos/'.$ImgBanner->img_nombre.'" class="img-responsive" /></a></div>';
                }

                OwlCarouselWidget::end();
                
            }
        }
    ?>
    <!-- Slider # 1: Fin -->

    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3350.22785291436!2d-68.83777268516296!3d-32.89214347646061!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x967e0917eef5d727%3A0x911bb2b4bd377b2!2sX+Nativa+GROUP!5e0!3m2!1ses!2sar!4v1563236419092!5m2!1ses!2sar" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>

</div>