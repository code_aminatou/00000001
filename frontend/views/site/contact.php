<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

use kartik\icons\Icon;
Icon::map($this);

$this->title = 'Contactar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">

    <div class="row">

        <div class="col-lg-3"></div>

        <div class="col-lg-6" style="background-color: #ffffff; margin: 0.5rem 0 1rem 0; border-radius: 2px; box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);">

            <div class="row">

                <div>

                    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                    <div style="padding: 24px;">

                        <h3><?= Icon::Show('envelope').Html::encode($this->title) ?></h3>
                        <hr>

                        <?= $form->field($model, 'name', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->textInput(['autofocus' => true])->label('Nombre') ?>

                        <?= $form->field($model, 'email', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->label('E-mail') ?>

                        <?= $form->field($model, 'subject', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->label('Asunto') ?>

                        <?= $form->field($model, 'body', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->textarea(['rows' => 6])->label('Mensaje') ?>

                        <?= $form->field($model, 'verifyCode', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->widget(Captcha::className(), [
                            'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                        ])->label('Código de verificación') ?>

                        <div class="form-group">
                            <p align="right"><?= Html::submitButton('Contactar', ['class' => 'btn btn-default', 'name' => 'contact-button']) ?></p>
                        </div>

                    </div>
                    <?php ActiveForm::end(); ?>

                </div>

            </div>

        </div>

        <div class="col-lg-3"></div>

    </div>
</div>
