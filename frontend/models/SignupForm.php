<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;

    public $nombre_real;
    public $apellido_real;
    public $dni;
    public $genero;
    public $rol;
    public $codigo_empresa;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required', 'message' => 'Por favor complete este campo.'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required', 'message' => 'Por favor complete este campo.'],
            ['email', 'email', 'message' => 'Por favor ingrese una dirección de e-mail válida.'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'El e-mail ingresado ya se encuentra registrado.'],

            ['password', 'required', 'message' => 'Por favor complete este campo.'],
            ['password', 'string', 'min' => 6],

            ['nombre_real', 'required', 'message' => 'Por favor complete este campo.'],
            ['nombre_real', 'string', 'min' => 2, 'max' => 255],

            ['apellido_real', 'required', 'message' => 'Por favor complete este campo.'],
            ['apellido_real', 'string', 'min' => 2, 'max' => 255],

            ['dni', 'required', 'message' => 'Por favor complete este campo.'],
            ['dni', 'integer', 'message' => 'Solo ingrese números.'],

            ['genero', 'required', 'message' => 'Por favor complete este campo.'],
            ['genero', 'integer', 'message' => 'Solo ingrese números.'],

            ['rol', 'required', 'message' => 'Por favor complete este campo.'],
            ['rol', 'integer', 'message' => 'Solo ingrese números.'],

            ['codigo_empresa', 'required', 'message' => 'Por favor complete este campo.'],
            ['codigo_empresa', 'integer', 'message' => 'Solo ingrese números.'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        $user->nombre_real = $this->nombre_real;        // <--- 000-000
        $user->apellido_real = $this->apellido_real;    // <--- 000-000
        $user->dni = $this->dni;                        // <--- 000-000
        $user->genero = $this->genero;                  // <--- 000-000
        $user->rol = $this->rol;                        // <--- 000-000
        $user->codigo_empresa = $this->codigo_empresa;  // <--- 000-000
        return $user->save() && $this->sendEmail($user);
    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
}
