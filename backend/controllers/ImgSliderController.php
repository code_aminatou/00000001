<?php

namespace backend\controllers;

use Yii;
use common\models\ImgSlider;
use common\models\Slider;
use common\models\ImgSliderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ImgSliderController implements the CRUD actions for ImgSlider model.
 */
class ImgSliderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ImgSlider models.
     * @return mixed
     */
    public function actionIndex($id_slider, $numero_slider)
    {
        $searchModel = new ImgSliderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andWhere(['numero_slider' => $numero_slider]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id_slider' => $id_slider,
            'numero_slider' => $numero_slider,
        ]);
    }

    /**
     * Displays a single ImgSlider model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ImgSlider model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_slider, $numero_slider)
    {
        $model = new ImgSlider();

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index', 'numero_slider' => $numero_slider]);
        }*/

        if ($model->load(Yii::$app->request->post())) {

            // Imagen
            $img_nombre = UploadedFile::getInstance($model, 'img_nombre');
            if (isset($img_nombre)) {
                $tmp = explode('.', $img_nombre->name);
                $ext = end($tmp);
                $model->img_nombre = Yii::$app->security->generateRandomString().".{$ext}";

                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/../archivos/';
                $path = Yii::$app->params['uploadPath'] . $model->img_nombre;
                $img_nombre->saveAs($path);
            }

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Datos guardados correctamente.');
                //return $this->redirect(['update', 'id' => $model->id]);
                return $this->redirect(['index', 'id_slider' => $id_slider, 'numero_slider' => $numero_slider]);
            }

        }

        return $this->render('create', [
            'model' => $model,
            'id_slider' => $id_slider,
            'numero_slider' => $numero_slider,
        ]);
    }

    /**
     * Updates an existing ImgSlider model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model_original = $this->findModel($id);
        $model = $this->findModel($id);

        $numero_slider = $model->numero_slider;

        $id_slider = Slider::find()->where(['numero_slider' => $numero_slider])->one();
        $id_slider = $id_slider->id;

        if ($model->load(Yii::$app->request->post())) {

            // Imagen
            $img_nombre = UploadedFile::getInstance($model, 'img_nombre');
            if (isset($img_nombre)) {
                $tmp = explode('.', $img_nombre->name);
                $ext = end($tmp);
                $model->img_nombre = Yii::$app->security->generateRandomString().".{$ext}";

                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/../archivos/';
                $path = Yii::$app->params['uploadPath'] . $model->img_nombre;
                $img_nombre->saveAs($path);
            } else {
                $model->img_nombre = $model_original->img_nombre;
            }

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Datos guardados correctamente.');
                //return $this->redirect(['update', 'id' => $model->id]);
                return $this->redirect(['index', 'id' => $model->id, 'id_slider' => $id_slider, 'numero_slider' => $numero_slider]);
            }

        }

        return $this->render('update', [
            'model' => $model,
            'id_slider' => $id_slider,
            'numero_slider' => $numero_slider,
        ]);
    }

    /**
     * Deletes an existing ImgSlider model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();

        $model = $this->findModel($id);
        $numero_slider = $model->numero_slider;

        $id_slider = Slider::find()->where(['numero_slider' => $numero_slider])->one();
        $id_slider = $id_slider->id;

        $model->delete();

        return $this->redirect(['index', 'id_slider' => $id_slider, 'numero_slider' => $numero_slider]);
    }

    /**
     * Finds the ImgSlider model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ImgSlider the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ImgSlider::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
