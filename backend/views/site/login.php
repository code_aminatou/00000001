<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use kartik\icons\Icon;
Icon::map($this);

$this->title = 'Iniciar sesión';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">

    <div class="row">

        <div class="col-lg-3"></div>

        <div id="div-login" class="col-lg-6" style="background-color: #ffffff; margin: 0.5rem 0 1rem 0; border-radius: 2px; box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);">
            <!--<h1><? //= Html::encode($this->title) ?></h1>-->
            <h3><?= Icon::show('sign-in-alt').Html::encode($this->title) ?></h3>
            <hr>

            <h6>

            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->textInput(['autofocus' => true])->label('Nombre de usuario') ?>

                <?= $form->field($model, 'password', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->passwordInput()->label('Contraseña') ?>

                <?= $form->field($model, 'rememberMe')->checkbox()->label('Recordar') ?>

                <div class="form-group">
                    <p align="right"><?= Html::submitButton('Iniciar sesión', ['class' => 'btn btn-default', 'name' => 'login-button']) ?></p>
                </div>

            <?php ActiveForm::end(); ?>

            </h6>
        </div>

        <div class="col-lg-3"></div>

    </div>

</div>
