<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ImgSlider */

$this->title = 'Agregar imagen a slider '.$numero_slider;
//$this->params['breadcrumbs'][] = ['label' => 'Img Sliders', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;

$this->params['breadcrumbs'][] = ['label' => 'Sliders', 'url' => ['slider/index']];
$this->params['breadcrumbs'][] = ['label' => 'Configuración (Slider '.$numero_slider.')', 'url' => ['slider/update', 'id' => $id_slider]];
$this->params['breadcrumbs'][] = ['label' => 'Agregar imagen a slider '.$numero_slider, 'url' => ['img-slider/index', 'id_slider' => $id_slider, 'numero_slider' => $numero_slider]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="img-slider-create">

    <h3><?= Html::encode($this->title) ?></h3>
    <hr>

    <?= $this->render('_form', [
        'model' => $model,
        'numero_slider' => $numero_slider,
    ]) ?>

</div>
