<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ImgSliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Imágenes de slider '.$numero_slider;
$this->params['breadcrumbs'][] = ['label' => 'Sliders', 'url' => ['slider/index']];
$this->params['breadcrumbs'][] = ['label' => 'Configuración (Slider '.$numero_slider.')', 'url' => ['slider/update', 'id' => $id_slider]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="img-slider-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!--<p align="right"><? //= Html::a('Cargar imagen a slider '.$numero_slider, ['create'], ['class' => 'btn btn-default']) ?></p>-->
        <p align="right"><?= Html::a('Agregar imagen a slider '.$numero_slider, ['create', 'id_slider' => $id_slider, 'numero_slider' => $numero_slider], ['class'=>'btn btn-default']) ?></p>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            
            //'img_nombre',
            [
                'attribute' => 'img_nombre',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::img(Yii::getAlias('@web').'/../../archivos/'. $data['img_nombre'], ['width' => '70px']);
                },
            ],

            'posicion',
            //'numero_slider',
            'url:url',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>


</div>
