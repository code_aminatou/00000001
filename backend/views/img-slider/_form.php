<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\ImgSlider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="img-slider-form">

    <?php
        if ($model->img_nombre == null) {
            $img_nombre = null;
        } else {
            $img_nombre = '../../archivos/'.$model->img_nombre;
        }
    ?>

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?php
        if (isset($model->numero_slider)) {
            $numero_slider_actual = $model->numero_slider;
        } else {
            $numero_slider_actual = $numero_slider;
        }
    ?>

    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($model, 'numero_slider')->textInput(['maxlength' => true, 'readonly' => true, 'value' => $numero_slider_actual])->label('N° de slider') ?>
            <!--<? //= $form->field($model, 'numero_slider')->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $numero_slider])->label(false); ?>-->
        </div>

        <div class="col-lg-3">
            <?= $form->field($model, 'posicion')->textInput()->label('Posición') ?>
        </div>
    </div>

    <!--<? //= $form->field($model, 'img_nombre')->textInput(['maxlength' => true])->label('Imagen') ?>-->
    <div class="row">
        <!-- Imagen -->
        <div class="col-lg-4">
            <?= $form->field($model, 'img_nombre')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'], 
                'pluginOptions' => [
                    'showClose' => false,
                    'showRemove' => false,
                    'showUpload' => false,
                    'showCaption' => false,

                    'initialPreview' => [
                        $img_nombre,
                    ],
                'initialPreviewAsData' => true,
                ],
            ])->label('Imagen'); ?>
        </div>
    </div>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true])->label('URL') ?>

    <hr>

    <div class="form-group">
        <p align="right"><?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?></p>
    </div>

    <?php ActiveForm::end(); ?>

</div>
