<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slider-form">

    <?php $form = ActiveForm::begin(); ?>

    <!--
    <? //= $form->field($model, 'numero_slider')->textInput() ?>

    <? //= $form->field($model, 'velocidad')->textInput() ?>

    <? //= $form->field($model, 'img_cantidad')->textInput() ?>

    <? //= $form->field($model, 'status')->textInput() ?>
	-->

	<div class="row">
		<div class="col-lg-4">
			<?= $form->field($model, 'numero_slider', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->textInput(['maxlength' => true])->label('Número de slider') ?>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-4">
			<?= $form->field($model, 'velocidad', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->textInput(['maxlength' => true])->label('Tiempo de transición (en segundos)') ?>
		</div>

		<div class="col-lg-4">
			<?= $form->field($model, 'img_cantidad', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->textInput(['maxlength' => true])->label('N° de imágenes visibles simultaneamente') ?>
		</div>

		<div class="col-lg-4">
			<?= $form->field($model, 'status', ['labelOptions'=>['style'=>'color:blue; font-size: 12px;']])->radioList(array('0' => 'Desactivado', '1' => 'Activado'))->label('Status') ?>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-4">
		    <div class="form-group">
		        <?= Html::submitButton('Guardar configuración', ['class' => 'btn btn-default']) ?>
		    </div>
		</div>
	</div>

    <?php ActiveForm::end(); ?>

    <?php
    	if (isset($model->numero_slider)) {
    ?>
	    <hr>
	    <?= Html::a('Imágenes cargadas en slider '.$model->numero_slider, ['img-slider/index', 'id_slider' => $model->id, 'numero_slider' => $model->numero_slider], ['class'=>'btn btn-default']) ?>
    <?php
    	}
    ?>

</div>
