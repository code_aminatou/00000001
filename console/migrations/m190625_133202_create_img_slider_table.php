<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%img_slider}}`.
 */
class m190625_133202_create_img_slider_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%img_slider}}', [
            'id' => $this->primaryKey(),
            'img_nombre' => $this->string(255)->notNull(),
            'posicion' => $this->integer()->notNull(),
            'numero_slider' => $this->integer()->notNull(),
            'url' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%img_slider}}');
    }
}
