<?php

use yii\db\Migration;

/**
 * Handles adding rol to table `{{%user}}`.
 */
class m190621_183754_add_rol_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'rol', $this->integer()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'rol');
    }
}
