<?php

use yii\db\Migration;

/**
 * Handles adding nombre_real to table `{{%user}}`.
 */
class m190621_183714_add_nombre_real_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'nombre_real', $this->string()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'nombre_real');
    }
}
