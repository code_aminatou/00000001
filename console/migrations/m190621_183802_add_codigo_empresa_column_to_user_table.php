<?php

use yii\db\Migration;

/**
 * Handles adding codigo_empresa to table `{{%user}}`.
 */
class m190621_183802_add_codigo_empresa_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'codigo_empresa', $this->integer()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'codigo_empresa');
    }
}
