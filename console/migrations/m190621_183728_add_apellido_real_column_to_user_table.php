<?php

use yii\db\Migration;

/**
 * Handles adding apellido_real to table `{{%user}}`.
 */
class m190621_183728_add_apellido_real_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'apellido_real', $this->string()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'apellido_real');
    }
}
