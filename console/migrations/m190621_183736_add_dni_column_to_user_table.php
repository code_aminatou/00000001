<?php

use yii\db\Migration;

/**
 * Handles adding dni to table `{{%user}}`.
 */
class m190621_183736_add_dni_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'dni', $this->integer()->notNull()->unique());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'dni');
    }
}
