<?php

use yii\db\Migration;

/**
 * Handles adding genero to table `{{%user}}`.
 */
class m190621_183746_add_genero_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'genero', $this->integer()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'genero');
    }
}
