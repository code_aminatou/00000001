<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ImgSlider;

/**
 * ImgSliderSearch represents the model behind the search form of `common\models\ImgSlider`.
 */
class ImgSliderSearch extends ImgSlider
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'posicion', 'numero_slider'], 'integer'],
            [['img_nombre', 'url'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ImgSlider::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'posicion' => $this->posicion,
            'numero_slider' => $this->numero_slider,
        ]);

        $query->andFilterWhere(['like', 'img_nombre', $this->img_nombre])
            ->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}
