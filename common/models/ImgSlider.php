<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "img_slider".
 *
 * @property int $id
 * @property string $img_nombre
 * @property int $posicion
 * @property int $numero_slider
 * @property string $url
 */
class ImgSlider extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'img_slider';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['posicion', 'numero_slider'], 'required'],
            [['posicion', 'numero_slider'], 'integer'],
            [['img_nombre', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'img_nombre' => 'Img Nombre',
            'posicion' => 'Posicion',
            'numero_slider' => 'Numero Slider',
            'url' => 'Url',
        ];
    }

    public static function getAll_ImgSlider($numero_slider) {
        return ImgSlider::find()->where(['numero_slider' => $numero_slider])->orderBy(['posicion'=>SORT_ASC])->all(); // Equivalente a SELECT * FROM BASE_DE_DATOS.img_banner WHERE numero_slider = 1; // SORT_ASC/SORT_DESC
    }
}
