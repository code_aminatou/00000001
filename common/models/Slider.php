<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property int $id
 * @property int $numero_slider
 * @property int $velocidad
 * @property int $img_cantidad
 * @property int $status
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero_slider', 'velocidad', 'img_cantidad', 'status'], 'required'],
            [['numero_slider', 'velocidad', 'img_cantidad', 'status'], 'integer'],
            [['numero_slider'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numero_slider' => 'Numero Slider',
            'velocidad' => 'Velocidad',
            'img_cantidad' => 'Img Cantidad',
            'status' => 'Status',
        ];
    }
}
