<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Se ha recibido una consulta.',
    'user.passwordResetTokenExpire' => 3600,
];
